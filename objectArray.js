// we can have array of objects. we can group together objects in an array. The array can use array methods for objects. However, objects are more complex than strings or numbers, there is a difference when handling them.

let users = [

	{
		name: "Mike Shell",
		username: "mikeBlueShell",
		email: "mikeyShell01@gmail.com",
		password: "iammikey1999",
		isActive: true,
		dateJoined: "August 8, 2011"
	},
	{
		name: "Jake Janella",
		username: "jjnella99",
		email: "jakejanella_99@gmail.com",
		password: "jakiejake12",
		isActive: true,
		dateJoined: "January 14,2015"
	}

];

console.log(users[0]);
console.log(users[1].email);

users[0].email = "mikeKingOfShells@gmail.com";
users[1].email = "janellajakeArchitect@gmail.com";

console.log(users);

users.push({

	name: "James Jameson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isActive: true,
	dateJoined: "February 14, 2000"
})

class User{
	constructor(name,username,email,password){

		this.name = name;
		this.username = username;
		this.email = email;
		this.password = password;
		this.isActive = true;
		this.dateJoined = "September 28, 2021";

	}
}

let newUser1 = new User("Kate Middletown","notTheDuchess","seriouslyNotDuchess@gmail.com","notRoyaltyAtAll");

console.log(newUser1);

users.push(newUser1);

console.log(users);

function login(username,password){

	let userFound = users.find((user)=>{

		// users Array is an array of objects

		return user.username === username && user.password === password
	})

	console.log(userFound);
	userFound ? alert(`Thank you for logging in, ${userFound.username}`) : alert(`Login Failed. Invalid Credentials.`);

}

// login("notTheDuchess","notRoyaltyAtAll");

// ACTIVITY

let courses = []

// Course Class Constructor
class Course{
	constructor(id,name,description,price){

		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
		this.isActive = true;

	}
}

//Create
const postCourse = (id,name,description,price) => {
	courses.push(new Course(id,name,description,price));
	alert(`You have created ${name}. The price is ${price}.`)
};

postCourse ("INTPROG","Introduction to Programming","intro to programming",12000);
postCourse ("ALGTRIG","Algebra and Trigonometry","algebra and trig",12000);

//Retrieve/Read
const getSingleCourse = id => {
	return courses.find((course)=>course.id === id);
};

// console.log(getSingleCourse("INTPROG"));

// Delete
const deleteCourse = () => courses.pop();